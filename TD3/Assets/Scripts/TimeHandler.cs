﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TimeHandler : MonoBehaviour {

    public Text zeitText;
    private float timer = 0;
    private bool start = false;
    public GameObject lvlHandler;
    public GameObject button;
    public GameObject[] buttonTower;
    public GameObject SceneButton;
    public GameObject TowerInfo;
    public Text ScoreEnd;
    public Text Stars;
    public float star1;
    public float star2;
    public float star3;

    private bool towerButton = false;

    public void startEnemy()
    {
        button.GetComponent<Button>().interactable = false;
        lvlHandler.GetComponent<LevelManager>().StartGame();
    }

    public void startTime()
    {
        start = true;

    }

    public void stopTime()
    {
        start = false;
        Stars.text = "";

        if(GameObject.Find("LobbyManager") == null)
        {
            ScoreEnd.text = "Your time is: " + timer;

            //TODO: Stars implement
            if (timer >= star1)
                Stars.text = "you got: *";
            if (timer >= star2)
                Stars.text = "you got: **";
            if (timer >= star3)
                Stars.text = "you got: ***";
        }
         



        SaveStars(Stars);
        SaveHighScore(timer, PlayerPrefs.GetString("PlayerName"));
        SceneButton.SetActive(true);
    }


    public void TowerControl()
    {
        towerButton = !towerButton;

        int i = 0;
        foreach (GameObject tower in lvlHandler.GetComponent<LevelManager>().getAvaTower())
        {
            buttonTower[i].SetActive(towerButton);
            buttonTower[i].GetComponent<Button>().GetComponentInChildren<Text>().text = tower.name + "\n" + "cost: " + tower.GetComponent<TowerCost>().getCost();
            i++;
        }

    }




    //----------------------HighScore Save---------------------------

    public void SaveHighScore(float score, string name)
    {
        float newScore = score, oldScore;
        string newName = name, oldName;
        int LevelNumb = (SceneManager.GetActiveScene().buildIndex - SceneManager.GetSceneByName("lala").buildIndex);

        //TODO:
        //Den Highscore an Stelle PlayerPrefs.GetFloat(NUMMER VON AKTUELLER SCENE - NUMMER DER SCENE VOM ERSTEN LEVE SCENE i + "HScore")
        for (int i = 0; i < 10; i++)
        {

            print("HSBefore" + LevelNumb + "" + i + "HScoreName" + ": " + PlayerPrefs.GetFloat(LevelNumb + "" + i + "HScore"));


            if (PlayerPrefs.HasKey(LevelNumb + "" + i + "HScore"))
            {
                if (PlayerPrefs.GetFloat(LevelNumb + "" + i + "HScore") < newScore)
                {
                    // new score is higher than the stored score
                    oldScore = PlayerPrefs.GetFloat(LevelNumb + "" + i + "HScore");
                    oldName = PlayerPrefs.GetString(LevelNumb + "" + i + "HScoreName");
                    PlayerPrefs.SetFloat(LevelNumb + "" + i + "HScore", newScore);
                    PlayerPrefs.SetString(LevelNumb + "" + i + "HScoreName", newName);
                    newScore = oldScore;
                    newName = oldName;
                    print("HS" + LevelNumb + "" + i + "HScoreName" + ": " + PlayerPrefs.GetFloat(LevelNumb + "" + i + "HScore"));
                }

            }
            else
            {
                PlayerPrefs.SetFloat(LevelNumb + "" + i + "HScore", newScore);
                PlayerPrefs.SetString(LevelNumb + "" + i + "HScoreName", newName);
                newScore = 0;
                newName = "";
            }
        }
    }
    //-----------------------------------------------------------------




    public void SaveStars(Text stars)
    {
        string LevelStars = PlayerPrefs.GetString(SceneManager.GetActiveScene().buildIndex + "stars");

        if (LevelStars == null)
        {
            PlayerPrefs.SetString(SceneManager.GetActiveScene().buildIndex + "stars", stars.text);
            PlayerPrefs.SetInt("Stars", PlayerPrefs.GetInt("Stars") + (stars.text.Length - LevelStars.Length));
        }

        else if (LevelStars.Length < stars.text.Length)
        {
            PlayerPrefs.SetString(SceneManager.GetActiveScene().buildIndex + "stars", stars.text);
            PlayerPrefs.SetInt("Stars", PlayerPrefs.GetInt("Stars") + (stars.text.Length - LevelStars.Length));
        }
        print(stars.text);
        print(PlayerPrefs.GetInt("Stars"));
    }






    // Update is called once per frame
    void Update () {

        if(start)
        {
            timer += Time.deltaTime;
            timer = Mathf.Round(timer * 100f) / 100f;
            zeitText.text = "" + timer;
        }
		
	}

    public bool getStart()
    {
        return start;
    }

    public float getTime()
    {
        return timer;
    }

    public GameObject getTowerInfoText()
    {
        return TowerInfo;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class TowerInformation : MonoBehaviour {
    GameObject TextPro;
    private bool TowerIsChoosen = false;
    
    // Use this for initialization
    void Start () {
      //  TextPro = GameObject.FindGameObjectWithTag("Text");
        TextPro = GameObject.Find("Canvas").GetComponent<TimeHandler>().getTowerInfoText();
    }

    
    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (TextPro.GetComponentInChildren<TextMeshProUGUI>() == null)
            {
                return;
            }
            else
            {
                if(this.gameObject.GetComponent<TowerCost>() == null)
                {
                    GameObject.Find("Canvas").GetComponent<SellTower>().setTower(this.transform.parent.gameObject);
                }
                else
                {
                    GameObject.Find("Canvas").GetComponent<SellTower>().setTower(this.gameObject);  
                }
                
                TextPro.SetActive(true);
                TextPro.GetComponentInChildren<TextMeshProUGUI>().SetText("Name: " + this.GetComponentInParent<TowerCost>().getName() + 
                                                                    "\nCost: " + this.GetComponentInParent<TowerCost>().getCost() + 
                                                                    "\nInfo: " + this.GetComponentInParent<TowerCost>().getInfo());
                
                TowerIsChoosen = true;
            }
        }
    }

    void OnMouseExit()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(1) && TowerIsChoosen)
        {
            TextPro.SetActive(false);
        }

    }
}

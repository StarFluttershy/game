﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReadScore : MonoBehaviour {
    public Text[] scores;
    int level = 0;
	// Use this for initialization

	void Start () {
        for(int i = 0; i < scores.Length;i++)
        {
            scores[i].text = "Platz " + (i+1) + ": " + PlayerPrefs.GetString(level.ToString() + i + "HScoreName") + " " + PlayerPrefs.GetFloat(level.ToString() + i+"HScore");
        }
	}
	
    public void nextHighscore()
    {
        level++;
        for (int i = 0; i < scores.Length; i++)
        {
            scores[i].text = "Platz " + (i + 1) + ": " + PlayerPrefs.GetString(level.ToString() + i + "HScoreName") + " " + PlayerPrefs.GetFloat(level.ToString() + i + "HScore");
        }

    }

    public void prevHighscore()
    {
        if(level != 0)
            level--;
        
        for (int i = 0; i < scores.Length; i++)
        {
            scores[i].text = "Platz " + (i + 1) + ": " + PlayerPrefs.GetString(level.ToString() + i + "HScoreName") + " " + PlayerPrefs.GetFloat(level.ToString() + i + "HScore");
        }

    }



    public void deleteScore()
    {   
        for (int i = 0; i < 10; i++)
        {
            PlayerPrefs.SetFloat(level + "" + i + "HScore", 0);
            PlayerPrefs.SetString(level + "" + i + "HScoreName", "");
        }
        this.Start();
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManaging : MonoBehaviour {

    public void NextScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void loadSceneWithLoader(int Scene)
    {
        PlayerPrefs.SetInt("LoadScene", Scene);
        SceneManager.LoadScene(5);
    }

    public void StartMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void LevelStart(int levelNummer)
    {
        SceneManager.LoadScene(levelNummer);
    }

    public void HighScore()
    {
        SceneManager.LoadScene(1);
    }

    public void LevelSelection()
    {
        SceneManager.LoadScene(2);
    }

    public void toShop()
    {
        SceneManager.LoadScene(3);
    }

    public void toNetwork()
    {
        //HIER NUMMER DER PVP MAP EINTRAGEN!!
        PlayerPrefs.SetInt("LoadScene", 8);
        SceneManager.LoadScene(4);
    }
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class LevelManager : MonoBehaviour
{
    public GameObject start;
    public GameObject end;
    private Vector3 fakeend;
    public GameObject[] tower;
    public GameObject ghostTower;
    public GameObject enemy;
    public GameObject time;
    public Text CurrencyText;
    public int lowestHeight;
    public int highestHeight;
    private int towerNumber = 0;
    public GameObject Ranking;

    public GameObject[] negativeTower;
    public int anzahlTower;
    public int Currency;
    private bool gameStarted = false;

    private GameObject e;
    private bool towerChoosen;
    private GameObject[] buildingArea;

    private List<GameObject> towers = new List<GameObject>();

    private List<GameObject> AvaTowers = new List<GameObject>();
    private bool canPlace = false;

    void Start() {
        CurrencyText.text = "Money: " + Currency;
        fakeend = end.transform.position;

        for (int i = 0; i < tower.Length; i++)
        {
            if (PlayerPrefs.GetString(i + "tower").Equals("true"))
                this.AvaTowers.Add(tower[i]);
        }

        if (buildingArea == null)
            buildingArea = GameObject.FindGameObjectsWithTag("Finish");

        gameStarted = true;
    }

    public void StartGame()
    {
        this.SpawnEnemy();
       
    }

    public List<GameObject> getTowerList()
    {
        return towers;
    }

    void Update()
    {
        StartCoroutine(PlaceTowers());

        if(e != null && 1.80 >= Vector3.Distance(end.transform.position, e.transform.position) && time.GetComponent<TimeHandler>().getStart())
        {
            time.GetComponent<TimeHandler>().stopTime();
            Ranking.SetActive(true);
            if (GameObject.Find("LobbyManager") != null)
                GameObject.Find("ScoreManager").GetComponent<Scoremega>().test(time.GetComponent<TimeHandler>().getTime());
        }
         
        if(Input.GetMouseButtonDown(1) && towerChoosen)
        {
            towerChoosen = false;
        }

    }

    public void lowerCurrency(int preis)
    {
        Currency -= preis;
        CurrencyText.text = "Money: " + Currency;
    }

    private Vector3 CheckPosition()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit = new RaycastHit();
        Vector3 newPos = hit.point;
        bool touching = false;

        if (towerChoosen)
        {
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                //Make sure to set towers in a grid, by rounding position to an int
                newPos = hit.point;

                if (ghostTower.GetComponent<GhostTower>().getPlane() != null)
                    newPos.Set(Mathf.RoundToInt(newPos.x), (ghostTower.GetComponent<GhostTower>().getPlane().GetComponent<Renderer>().bounds.min.y - 0.1F), Mathf.RoundToInt(newPos.z));
                else
                    newPos.Set(Mathf.RoundToInt(newPos.x), hit.point.y, Mathf.RoundToInt(newPos.z));

                ghostTower.transform.position = newPos;


                bool noWall = true;

                foreach(GameObject tow in towers)
                {
                    if (0.5 >= Vector3.Distance(ghostTower.transform.position, tow.transform.position))
                        noWall = false;
                }

                if ((ghostTower.GetComponent<GhostTower>().getTouching() && noWall && Currency >= AvaTowers[towerNumber].GetComponent<TowerCost>().getCost()))
                {
                    ghostTower.GetComponent<Renderer>().material.color = Color.green;
                    canPlace = true;
                }
                else
                {
                    ghostTower.GetComponent<Renderer>().material.color = Color.red;
                    canPlace = false;
                }

            }
            else
            {
                ghostTower.GetComponent<Renderer>().material.color = Color.red;
                canPlace = false;
            }
        }else
        {
            canPlace = false;
            Vector3 pos = new Vector3(1000,1000,1000);
            ghostTower.transform.position = pos;
        }

        //Return all hit information which we use later
        return newPos;
    }

    private IEnumerator PlaceTowers()
    {
        Vector3 hit = CheckPosition();

        if (hit != null && Input.GetMouseButtonDown(0) && canPlace)
        {
            Debug.Log("plazieren");
            GameObject newTower = Instantiate(AvaTowers[towerNumber], new Vector3(Mathf.RoundToInt(hit.x), hit.y, Mathf.RoundToInt(hit.z)), Quaternion.identity) as GameObject;
            towers.Add(newTower);
            yield return new WaitForEndOfFrame();
            this.lowerCurrency(AvaTowers[towerNumber].GetComponent<TowerCost>().getCost());
            Pathfinder.Instance.InsertInQueue(start.transform.position, end.transform.position, CheckRoute);
        }
    }

    private void CheckRoute(List<Vector3> list)
    {
        //If we get a list that is empty there is no path, and we blocked the road
        //Then remove the last added tower!
        if (list.Count < 1 || list == null)
        {
            if (towers.Count > 0)
            {
                GameObject g = towers[towers.Count - 1];
                towers.RemoveAt(towers.Count - 1);
                this.lowerCurrency(-AvaTowers[towerNumber].GetComponent<TowerCost>().getCost());
                Destroy(g);
            }
        }
    }


//-----Tower removal for the random placement at the beginning;
    public void CheckRouteForRandom()
    {
        Pathfinder.Instance.InsertInQueue(start.transform.position, end.transform.position, CheckRouteForRandomForReal);
    }

    private void CheckRouteForRandomForReal(List<Vector3> list)
    {
        if (list.Count < 1 || list == null)
        {
            if (towers.Count > 0)
            {
                GameObject g = towers[towers.Count - 1];
                towers.RemoveAt(towers.Count - 1);
                Destroy(g);
            }
        }
    }
    //-----Tower removal for the random placement at the beginning;



    public void SwapStartAndEnd(GameObject player)
    {
        Vector3 temp;

        temp = start.transform.position;
        start.transform.position = fakeend;
        fakeend = temp;
        player.GetComponent<Pathfinding>().FindPath(start.transform.position, fakeend);
    }




    void SpawnEnemy()
    {
        e = Instantiate(enemy, start.transform.position, Quaternion.identity) as GameObject;
        e.GetComponent<PlayerCap>().start = start.transform.position;
        e.GetComponent<PlayerCap>().end = end.transform.position;
        time.GetComponent<TimeHandler>().startTime();
    }

    public void ChooseTower(int tower)
    {
        GameObject.Find("Canvas").GetComponent<TimeHandler>().getTowerInfoText().SetActive(false);
        this.towerNumber = tower;
        this.towerChoosen = true;
    }

    public void makeTChooseFalse()
    {
        this.towerChoosen = false;
    }

    public List<GameObject> getAvaTower() {
        return AvaTowers;
    }

    public bool getGameStarted()
    {
        return gameStarted;
    }

    public List<GameObject> getTowers()
    {
        return towers;
    }

    public bool isPosAva(Vector3 pos)
    {
        bool check = true; 

        foreach(GameObject tower in towers)
        {
            if (tower.transform.position == pos)
                check = false;
        }
        return check;
    }

    public GameObject[] getNegativeTower()
    {
        return negativeTower;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCam : MonoBehaviour
{
    public float speed = 20f;
    public float BoarderThicccness = 20f;
    public Vector2 limit;
    public GameObject Map;
    public Camera cam;

    void Update()
    {
        Vector3 pos = transform.position;
        Vector3 posMap = Map.transform.position;
        

        List < Vector3 > corner = this.centersOfEdges(Map);
        

        if(Input.GetKey("w") || Input.mousePosition.y >= Screen.height - BoarderThicccness)
        {
            pos.z += speed * Time.deltaTime;
        }

        if (Input.GetKey("s") || Input.mousePosition.y <= BoarderThicccness)
        {
            pos.z -= speed * Time.deltaTime;
        }

        if (Input.GetKey("d") || Input.mousePosition.x >= Screen.width - BoarderThicccness)
        {
            pos.x += speed * Time.deltaTime;
        }

        if (Input.GetKey("a") || Input.mousePosition.x <= BoarderThicccness)
        {
            pos.x -= speed * Time.deltaTime;
        }

        
        
        pos.x = Mathf.Clamp(pos.x, corner[3].x , corner[2].x);
        pos.z = Mathf.Clamp(pos.z, corner[1].y, corner[0].y);

        transform.position = pos;
    }


    private List<Vector3> corners(GameObject go)
    {
        float width = go.GetComponent<Renderer>().bounds.size.x;
        float height = go.GetComponent<Renderer>().bounds.size.y;

        Vector3 topRight = go.transform.position, topLeft = go.transform.position, bottomRight = go.transform.position, bottomLeft = go.transform.position;

        topRight.x += width / 2;
        topRight.y += height / 2;

        topLeft.x -= width / 2;
        topLeft.y += height / 2;

        bottomRight.x += width / 2;
        bottomRight.y -= height / 2;

        bottomLeft.x -= width / 2;
        bottomLeft.y -= height / 2;

        List<Vector3> cor_temp = new List<Vector3>();
        cor_temp.Add(topRight);
        cor_temp.Add(topLeft);
        cor_temp.Add(bottomRight);
        cor_temp.Add(bottomLeft);

        return cor_temp;
    }
    private List<Vector3> centersOfEdges(GameObject go)
    {
        float width = go.GetComponent<Renderer>().bounds.size.x;
        float height = go.GetComponent<Renderer>().bounds.size.y;

        Vector3 top = go.transform.position, bottom = go.transform.position, right = go.transform.position, left = go.transform.position;

        top.y += height / 2;

        bottom.y -= height / 2;

        left.x -= width / 2;

        right.x += width / 2;

        List<Vector3> ed_temp = new List<Vector3>();
        ed_temp.Add(top);
        ed_temp.Add(bottom);
        ed_temp.Add(right);
        ed_temp.Add(left);

        return ed_temp;
    }

}
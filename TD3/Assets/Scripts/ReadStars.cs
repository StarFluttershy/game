﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReadStars : MonoBehaviour {

    public Button[] Tower;

    public Text Stars;
	
    // Use this for initialization
    void Start () {

        for(int i = 0; i < Tower.Length;i++)
        {
            if(PlayerPrefs.GetString(i+"tower").Equals("true"))
            {
                Tower[i].interactable = false;
            }
        }
	}

	// Update is called once per frame
	void Update () {
        Stars.text = PlayerPrefs.GetInt("Stars").ToString();
    }



    public void disableAll()
    {
        for (int i = 0; i < Tower.Length; i++)
        {
            PlayerPrefs.SetString(i + "tower", "false");
            Tower[i].interactable = true;
        }

        PlayerPrefs.SetInt("Stars", 220);
    }

    public void buyTower(int tower)
    {
        if(PlayerPrefs.GetInt("Stars") > 5*tower)
        {
            PlayerPrefs.SetInt("Stars", PlayerPrefs.GetInt("Stars") - (5 * tower));
            PlayerPrefs.SetString(tower-1 + "tower", "true");
            Tower[tower-1].interactable = false;
        }      
    }


}

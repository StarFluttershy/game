﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostTower : MonoBehaviour
{


    private bool touching = false;
    private GameObject plane;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Building")
        {
            touching = true;
            plane = collision.gameObject;
        }
    }

    void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Building")
            touching = true;
    }




    void OnCollisionExit(Collision collision)
    {

        if (collision.gameObject.tag == "Building" && collision.gameObject.tag != "Wall")
            touching = false;
    }

    public bool getTouching()
    {
        return touching;
    }

    public GameObject getPlane()
    {
        return plane;
    }
}

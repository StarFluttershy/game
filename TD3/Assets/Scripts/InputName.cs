﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputName : MonoBehaviour {


    public GameObject NameBereich;
    public Text NameChangeButtenText;
    public InputField newName;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
        if (PlayerPrefs.GetString("PlayerName") == "" || PlayerPrefs.GetString("PlayerName") == null)
        {
            NameChangeButtenText.text = "name?";
        }

        else
        {
            NameChangeButtenText.text = PlayerPrefs.GetString("PlayerName");
        }
    }


    public void OpenCloseName()
    {
        NameBereich.SetActive(!NameBereich.activeSelf);
    }

    public void ChangeName(string name)
    {
        PlayerPrefs.SetString("PlayerName", newName.text);
    }
}

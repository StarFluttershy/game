﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomTowerPlacment : MonoBehaviour {

    public GameObject levelmanager;
    private bool placed = false;
    public int min;
    public int max;
    public int chanceAufBadTower;


    // Use this for initialization
    void Start () {
        if (!(min >= 0 && max >= min))
        {
            max = 10;
            min = 2;
        }

     //   Mathf.RoundToInt(newPos.x), newPos.y, Mathf.RoundToInt(newPos.z)
     //  GameObject newTower = Instantiate(AvaTowers[towerNumber], new Vector3(Mathf.RoundToInt(hit.point.x), hit.point.y, Mathf.RoundToInt(hit.point.z)), Quaternion.identity) as GameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (!placed && min != 0 && max != 0 && levelmanager.GetComponent<LevelManager>().getGameStarted() )
        {
            placed = true;
            GameObject[] places = GameObject.FindGameObjectsWithTag("Building"), badTower = levelmanager.GetComponent<LevelManager>().getNegativeTower();
            GameObject Newtower;
            List<GameObject> avaTower = levelmanager.GetComponent<LevelManager>().getAvaTower();

            int numb;
            int roll;
            Vector3 newPos;

            
            if (places != null)
            {
                foreach (GameObject place in places)
                {
                    Vector3 PlaneMax = place.GetComponent<Renderer>().bounds.max;
                    Vector3 PlaneMin = place.GetComponent<Renderer>().bounds.min;
                    Vector3 lengh = (PlaneMax - PlaneMin) / 2;
                    Vector3 Pos = place.transform.position;
                    numb = Random.Range(min, max);

                    for (int i = 0; i < numb; i++)
                    {
                        int j = 0;
                        do
                        {
                            j++;
                            newPos = new Vector3(
                            Mathf.RoundToInt((Pos.x + (lengh.x * Random.Range(-1f, 1f)))),
                            (place.GetComponent<Renderer>().bounds.min.y),
                            Mathf.RoundToInt((Pos.z + (lengh.z * Random.Range(-1f, 1f)))));
                        }while (j < 10 && !levelmanager.GetComponent<LevelManager>().isPosAva(newPos));

                        if (levelmanager.GetComponent<LevelManager>().isPosAva(newPos)){
                            roll = Random.Range(0, 100);
                            if (roll > chanceAufBadTower && badTower != null)
                            {
                                Newtower = Instantiate(badTower[Random.Range(0, badTower.Length - 1)], new Vector3(newPos.x, newPos.y, newPos.z), Quaternion.identity) as GameObject; Random.Range(0, avaTower.Count - 1);
                            }
                            else
                            {
                                Newtower = Instantiate(avaTower[Random.Range(0, avaTower.Count - 1)], new Vector3(newPos.x, newPos.y, newPos.z), Quaternion.identity) as GameObject;
                            }

                            levelmanager.GetComponent<LevelManager>().getTowers().Add(Newtower);
                            levelmanager.GetComponent<LevelManager>().CheckRouteForRandom();
                        }

                        for (int k = 0; k < 5; k++)
                        {
                            newPos = new Vector3((newPos.x + Random.Range(-1, 1)), newPos.y, (newPos.y + Random.Range(-1, 1)));
                            if (levelmanager.GetComponent<LevelManager>().isPosAva(newPos) && newPos.x - PlaneMax.x < 0 && newPos.z - PlaneMax.z < 0 && newPos.x - PlaneMin.x > 0 && newPos.z - PlaneMin.z > 0)
                            {

                                roll = Random.Range(0, 100);
                                if (roll > chanceAufBadTower && badTower != null)
                                {
                                    Newtower = Instantiate(badTower[Random.Range(0, badTower.Length - 1)], new Vector3(newPos.x, newPos.y, newPos.z), Quaternion.identity) as GameObject; Random.Range(0, avaTower.Count - 1);
                                }
                                else
                                {
                                    Newtower = Instantiate(avaTower[Random.Range(0, avaTower.Count - 1)], new Vector3(newPos.x, newPos.y, newPos.z), Quaternion.identity) as GameObject;
                                }

                                levelmanager.GetComponent<LevelManager>().getTowers().Add(Newtower);
                                levelmanager.GetComponent<LevelManager>().CheckRouteForRandom();
                                i++;
                            }
                        }
                       // levelmanager.GetComponent<LevelManager>().CheckRouteForRandom();
                    }
                }
            }
            this.gameObject.SetActive(false);
        }
    }
}

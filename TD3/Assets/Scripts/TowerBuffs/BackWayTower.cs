﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackWayTower : MonoBehaviour {

    [SerializeField]
    private int Raduis;
    [SerializeField]
    private float buffTime;

    private float timer;
    private bool startTime = false;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!startTime)
        {

            Collider[] cols = Physics.OverlapSphere(transform.position, Raduis);
            foreach (Collider c in cols)
            {
                // TDEnemy e = c.GetComponent<TDEnemy>();
                if (c.gameObject.tag == "Player")
                {
                    startTime = true;
                    c.GetComponent<PlayerCap>().RuckwertsLaufen(buffTime);
                }
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StunTower : MonoBehaviour
{

    public int slowRaduis = 1;
    [SerializeField]
    private float Stuntime = 1f;

    private float timer;
    private bool startTime = false;
    
    GameObject stuned;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (startTime)
        {
            timer += Time.deltaTime;
        }

        if(timer >= 5)
        {
            timer = 0;
            startTime = false;
        }

        if (!startTime)
        {
            Collider[] cols = Physics.OverlapSphere(transform.position, slowRaduis);
            foreach (Collider c in cols)
            {
                // TDEnemy e = c.GetComponent<TDEnemy>();
                if (c.gameObject.tag == "Player")
                {
                    startTime = true;
                    stuned = c.gameObject;
                    stuned.GetComponent<PlayerCap>().Stun(Stuntime);
                    stuned.transform.GetChild(2).gameObject.SetActive(true);
                    Invoke("StopStunAnimation", Stuntime);
                }
            }
        }

    }

    public void StopStunAnimation()
    {
        stuned.transform.GetChild(2).gameObject.SetActive(false);
    }
   
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowTower : MonoBehaviour {

    [SerializeField]
    private int slowRaduis;
    [SerializeField]
    private float slowspeed;
    [SerializeField]
    private float slowTime;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        
        Collider[] cols = Physics.OverlapSphere(transform.position, slowRaduis);
        foreach(Collider c in cols){
            // TDEnemy e = c.GetComponent<TDEnemy>();
            if (c.gameObject.tag == "Player")
            {
                Debug.Log("now");
                c.GetComponent<PlayerCap>().slow(slowspeed, slowTime);
            }
        }
	}
}

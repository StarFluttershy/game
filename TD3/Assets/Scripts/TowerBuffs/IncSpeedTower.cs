﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncSpeedTower : MonoBehaviour {

    [SerializeField]
    private int Raduis;
    [SerializeField]
    private float speed;
    [SerializeField]
    private float buffTime;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


        Collider[] cols = Physics.OverlapSphere(transform.position, Raduis);
        foreach (Collider c in cols)
        {
            // TDEnemy e = c.GetComponent<TDEnemy>();
            if (c.gameObject.tag == "Player")
            {
                c.GetComponent<PlayerCap>().slow(speed, buffTime);
            }
        }
    }
}

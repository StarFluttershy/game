﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class HideNetworkHUD : NetworkBehaviour {

	// Use this for initialization
	void Start () {
        if (GameObject.FindObjectOfType<NetworkManagerHUD>() != null)
            GameObject.FindObjectOfType<NetworkManagerHUD>().showGUI = false;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}

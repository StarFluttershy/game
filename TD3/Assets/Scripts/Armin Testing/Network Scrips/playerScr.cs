﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class playerScr : NetworkBehaviour {
    
    int i = 0;
    float timeing;
    public Text text;
    private string Playername;
   
    void Start()
    {

        if (isLocalPlayer)
        {
            text.transform.position = this.gameObject.transform.position;
            CmdSetName(PlayerPrefs.GetString("PlayerName"));
            //CmdSetName();
        }
        
     //   playercounter = GameObject.FindGameObjectsWithTag("text2").Length - 1;

    }

    private void Update()
    {
        if (isLocalPlayer)
        {
            
            CmdSetName(PlayerPrefs.GetString("PlayerName"));
            if (GameObject.Find("ScoreManager") != null)
                GameObject.Find("ScoreManager").GetComponent<Scoremega>().pl = this;
        }
        Cmdst();
    }

    [Command]
    void CmdDamagePlayer(float time)
    {
        RpcDamagePlayer(time);

    }

    [Command]
    void CmdSetName(string PN)
    {
        RpcSetName(PN);
    }

    [ClientRpc]
    void RpcSetName(string PN)
    {
        Playername = PN;
    }



         [Command]
    void Cmdst()
    {
        RpcTextOnly();
    }

    [ClientRpc]
    void RpcDamagePlayer(float time)
    {
        timeing = time;

        text.transform.position = this.gameObject.transform.position;
        if (timeing != 0)
            text.text = Playername + ": " + timeing;

        else{
            text.text = "";
        }
    }

    [ClientRpc]
    void RpcTextOnly()
    {
        text.transform.position = this.gameObject.transform.position;

        if (timeing != 0)
            text.text = Playername + ": " + timeing;

        else
        {
            text.text = "";
        }
    }

    public void Foooooooooooooooooooooooooo(float time)
    {
        
        //If we are the server, do the Rpc. If we are a client, do the Command.
        if (!isServer)
            CmdDamagePlayer(time);
        else
            RpcDamagePlayer(time);
    }
}

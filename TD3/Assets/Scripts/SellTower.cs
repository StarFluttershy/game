﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SellTower : MonoBehaviour {

    private GameObject SelectedTower;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void sellTower()
    {
        GameObject.Find("LevelHandler").GetComponent<LevelManager>().lowerCurrency(-SelectedTower.GetComponent<TowerCost>().cost);
        GameObject.Find("Canvas").GetComponent<TimeHandler>().getTowerInfoText().SetActive(false);
        SelectedTower.SetActive(false);


        List<GameObject> towers = GameObject.Find("LevelHandler").GetComponent<LevelManager>().getTowerList();
        towers.Remove(SelectedTower);
        Destroy(SelectedTower);
    }

    public void setTower(GameObject tower)
    {
        SelectedTower = tower;
    }

}

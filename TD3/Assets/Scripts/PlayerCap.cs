﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCap : Pathfinding
{

    public Vector3 start = Vector3.zero;
    public Vector3 end = Vector3.zero;


    private bool pathMover = true;
    private bool newPath = true;

    private bool slowing = false;
    private bool stunning = false;
    private bool fastBuff = false;
    private bool backWay = false;

    public float MoventSpeed = 5;

    private float timerSlow = 0;
    private float timerStun = 0;
    private float timerFast = 0;
    private float timerBack = 0;

    private float speedSlow = 1;
    private float speedStun = 1;
    private float speedFast = 1;
    private float backWalkSpeed = 1;
    
    private float StunTime;
    private float slowTime;
    private float buffTime;
    private float backTime;


    void Start()
    {

    }
    // Update is called once per frame
    void Update() {
        if (transform.position != end)
        {
            if (start != Vector3.zero && end != Vector3.zero && newPath)
            {
                StartCoroutine(PathTimer());
            }

            Movement();
        }
        else
        {
            DestroyImmediate(gameObject);
        }

        if (slowing)
        {
            timerSlow += Time.deltaTime;
            
            if (timerSlow >= slowTime)
            {
                speedSlow = 1;
                timerSlow = 0;
                slowing = false;
            }

        }


        if (stunning)
        {
            timerStun += Time.deltaTime;

            if (timerStun >= StunTime)
            {
                speedStun = 1;
                timerStun = 0;
                slowing = false;
            }

        }

        if (fastBuff)
        {
            timerFast += Time.deltaTime;

            if (timerFast >= buffTime)
            {
                speedFast = 1;
                timerFast = 0;
                fastBuff = false;
            }

        }

        if (backWay)
        {
            timerBack += Time.deltaTime;

            if (timerBack >= backTime)
            {
                GameObject.Find("LevelHandler").GetComponent<LevelManager>().SwapStartAndEnd(this.gameObject);
                timerBack = 0;
                backWay = false;
            }

        }
    }

    public void slow(float slowspeed, float slow)
    {
        this.speedSlow = slowspeed;
        slowing = true;
        slowTime = slow;
    }

    public void RuckwertsLaufen(float backTime)
    {
        GameObject.Find("LevelHandler").GetComponent<LevelManager>().SwapStartAndEnd(this.gameObject);
        backWay = true;
        this.backTime = backTime;
    }

    public void BuffFast(float fastSpeed, float buffTime)
    {
        this.speedFast = fastSpeed;
        fastBuff = true;
        this.buffTime = buffTime;
    }

    public void Stun(float StunTime)
    {
        this.speedStun = 0;
        stunning = true;
        this.StunTime = StunTime;
    }


    IEnumerator PathTimer()
    {
        newPath = false;
        FindPath(transform.position, end);
        yield return new WaitForSeconds(0.5F);
        newPath = true;
    }



    private void Movement()
    {
        if (Path.Count > 0)
        {

            if (pathMover)
            {
                //StartCoroutine(PathRemoval(4F + 2F));
            }

            if (Vector3.Distance(transform.position, new Vector3(Path[0].x, transform.position.y, Path[0].z)) < 0.2F)
            {
                Path.RemoveAt(0);
            }


            if (Path.Count > 0)
            {
                Vector3 direction = (new Vector3(Path[0].x, transform.position.y, Path[0].z) - transform.position).normalized;
                if (direction == Vector3.zero)
                {
                    // direction = (end - transform.position).normalized;
                }
                transform.position = Vector3.MoveTowards(transform.position, transform.position + direction, (Time.deltaTime * MoventSpeed * (speedFast * speedSlow * speedStun)));
            }
        }
    }


    IEnumerator PathRemoval(float speed)
    {
        pathMover = false;
        yield return new WaitForSeconds((1 * Pathfinder.Instance.Tilesize) / speed);
        if (Path.Count > 0)
        {
            Path.RemoveAt(0);
        }
        pathMover = true;
    }




}
